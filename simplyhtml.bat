@echo off

REM ==================================
REM Batch for starting jHepWork on Windows
REM ==================================

REM ==================================
REM SET JEHEP_HOME TO THE INSTALLATION DIRECTORY 
REM The default is the current directory
REM If you install in in some other location, set it like this 
REM set JEHEP_HOME=C:\jHepWork-bin-1.0\jHepWork


REM current directory
set JEHEP_HOME=.
set JEHEP_HOME_PATH=%CD%
echo Current directory %JEHEP_HOME_PATH%


set MAX_JAVA_MEMORY=256

set CMD_LINE_ARGS=
:args
if "%1"=="" goto start
set CMD_LINE_ARGS=%CMD_LINE_ARGS% %1
shift
goto args
echo ARGS: %CMD_LINE_ARGS%

:start
set JEHEP_JAR=%JEHEP_HOME%\ekit.jar
set JEHEP_CLASSPATH=
if exist %JEHEP_JAR% set JEHEP_CLASSPATH=%JEHEP_JAR%

REM take first the main class
set LOCALCLASSPATH=%JEHEP_CLASSPATH%


REM link  LIBRARIES in lib directory
for %%i in ("%JEHEP_HOME%\lib\*.jar") do call "%JEHEP_HOME%\lcp.bat" %%i


REM TAKE ALL
set CLASSPATH=%LOCALCLASSPATH%
set LOCALCLASSPATH=


REM pause

echo Starting JHEPWORK from from %JEHEP_CLASSPATH%...
java -mx%MAX_JAVA_MEMORY%m -classpath %CLASSPATH% -Djscipad.home=%JEHEP_HOME% com.hexidec.ekit.Ekit %CMD_LINE_ARGS%

